import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:universal_platform/universal_platform.dart';

class StaticSite extends StatelessWidget {
  final String data;

  StaticSite({this.data});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: UniversalPlatform.isMacOS || UniversalPlatform.isWindows || UniversalPlatform.isFuchsia
            ? const Center(
                child: Text('This platform is not support for webview'),
              )
            : WebView(
                onWebViewCreated: (controller) async {
                  await controller
                      .loadUrl('data:text/html;base64,$data');
                },
              ),
      ),
    );
  }
}
