import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/tools.dart';
import '../../../models/app.dart';
import '../../../models/blog_news.dart';
import '../../../screens/base.dart';
import '../../../services/wordpress.dart';
import '../../../widgets/blog/detailed_blog/detailed_blog_view.dart';
import '../../../widgets/webview.dart';

/// The Banner type to display the image
class BannerImageItem extends StatefulWidget {
  final Key key;
  final config;
  final double width;
  final padding;
  final BoxFit boxFit;
  final radius;

  BannerImageItem(
      {this.key,
      this.config,
      this.padding,
      this.width,
      this.boxFit,
      this.radius})
      : super(key: key);

  @override
  _BannerImageItemState createState() => _BannerImageItemState();
}

class _BannerImageItemState extends BaseScreen<BannerImageItem> {
//  BlogNews _blog;

  List<BlogNews> _blogs = [];

  final WordPress _service = WordPress();

  @override
  void afterFirstLayout(BuildContext context) async {
    if (widget.config["category"] != null) {
      await _service
          .fetchBlogLayout(
              config: widget.config,
              lang: Provider.of<AppModel>(context, listen: false).locale)
          .then((blogs) {
        setState(() {
          _blogs = blogs;
          print(_blogs);
        });
      });
    }
    if (widget.config["blog"] != null) {
      await _service.getBlog(widget.config["blog"]).then((blog) {
        setState(() {
          _blogs.add(blog);
        });
      });
    }
  }

  _onTap(context) {
    /// support to show the post detail
    if (widget.config["blog"] != null && _blogs.isNotEmpty) {
      return Navigator.push(
          context,
          MaterialPageRoute<void>(
            builder: (BuildContext context) =>
                getDetailBlog(_blogs[0], context),
            fullscreenDialog: true,
          ));
    }
    if (widget.config["url"] != null) {
      return Navigator.push(
          context,
          MaterialPageRoute<void>(
            builder: (BuildContext context) =>
                WebView(url: widget.config["url"]),
            fullscreenDialog: true,
          ));
    }
    //if (widget.config["post"] != null) {}

    /// Default navigate to show the list products
    BlogNews.showList(context: context, config: widget.config, blogs: _blogs);
  }

  @override
  Widget build(BuildContext context) {
    double _padding =
        Tools.formatDouble(widget.config["padding"] ?? widget.padding ?? 10.0);
    double _radius = Tools.formatDouble(widget.config['radius'] ?? 0.0);

    final screenSize = MediaQuery.of(context).size;
    final screenWidth =
        screenSize.width / (2 / (screenSize.height / screenSize.width));
    final itemWidth = widget.width ?? screenWidth;

    return GestureDetector(
      onTap: () => _onTap(context),
      child: Container(
        width: itemWidth,
        child: Padding(
            padding: EdgeInsets.only(left: _padding, right: _padding),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(_radius),
              child: Tools.image(
                fit: widget.boxFit ?? BoxFit.fitWidth,
                url: widget.config["image"],
              ),
            )),
      ),
    );
  }
}
