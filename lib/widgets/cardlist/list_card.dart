import 'package:flutter/material.dart';

import '../../models/blog_news.dart';
import '../../widgets/blog_news/blog_card_view.dart';

class ListCard extends StatelessWidget {
  final List<BlogNews> data;
  final String id;
  final width;

  ListCard({this.data, this.id, this.width});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        double _width = width;

        return Container(
          height: _width * 0.4 + 120,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            key: ObjectKey(id),
            itemBuilder: (context, index) {
              return BlogCard(item: data[index], width: _width * 0.35);
            },
            itemCount: data.length,
          ),
        );
      },
    );
  }
}
