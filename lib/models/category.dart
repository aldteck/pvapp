import 'package:flutter/material.dart';

import '../common/constants.dart';
import '../services/wordpress.dart';

class CategoryModel with ChangeNotifier {
  final WordPress _service = WordPress();
  List<Category> categories;
  Map<int, Category> categoryList = {};

  bool isLoading = true;
  String message;

  void sortCategoryList(
      {List<Category> categoryList,
      dynamic sortingList,
      String categoryLayout}) {
    List<Category> result = categoryList;

    if (sortingList != null) {
      List<Category> _categories = [];
      List<Category> _subCategories = [];
      bool isParent = true;
      for (var category in sortingList) {
        Category item = categoryList.firstWhere(
            (Category cat) => cat.id.toString() == category.toString(),
            orElse: () => null);
        if (item != null) {
          if (item.parent.toString() != '0') {
            isParent = false;
          }
          _categories.add(item);
        }
      }
      if (!['column', 'grid', 'subCategories'].contains(categoryLayout)) {
        for (var category in categoryList) {
          Category item = _categories.firstWhere((cat) => cat.id == category.id,
              orElse: () => null);
          if (item == null && isParent && category.parent.toString() != '0') {
            _subCategories.add(category);
          }
        }
      }
      result = [..._categories, ..._subCategories];
    }
    categories = result;
    notifyListeners();
  }

  void getCategories({lang, sortingList, categoryLayout}) async {
    try {
      categories = await _service.getCategories(lang: lang);
      sortCategoryList(
          categoryList: categories,
          sortingList: sortingList,
          categoryLayout: categoryLayout);
      isLoading = false;

      for (Category cat in categories) {
        categoryList[cat.id] = cat;
      }

      print('This is list of your categories id');
      print(categories);

      notifyListeners();
    } catch (err) {
      isLoading = false;
      message = err.toString();
      notifyListeners();
    }
  }
}

class Category {
  int id;
  String name;
  String image;
  int parent;
  int totalProduct;

  Category.fromJson(Map<String, dynamic> parsedJson) {
    if (parsedJson["slug"] == 'uncategorized') {
      return;
    }

    id = parsedJson["id"];
    name = parsedJson["name"];
    parent = parsedJson["parent"];
    totalProduct = parsedJson["count"];

    final image = parsedJson["image"];
    if (image != null) {
      this.image = image["src"].toString();
    } else {
      this.image = kDefaultImage;
    }
  }

  @override
  String toString() => 'Category { id: $id  name: $name}';
}
