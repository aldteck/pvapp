// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a hi locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'hi';

  static m0(day) => "${day} दिन पहले";

  static m1(hour) => "${hour} घंटे पहले";

  static m2(minute) => "${minute} मिनट पहले";

  static m3(month) => "${month} महीने पहले";

  static m4(percent) => "बिक्री ${percent} %";

  static m5(second) => "${second} सेकंड पहले";

  static m6(length) => "हमें ${length} उत्पाद मिले";

  static m7(year) => "${year} साल पहले";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "aboutUs" : MessageLookupByLibrary.simpleMessage("हमारे बारे में"),
    "addANewPost" : MessageLookupByLibrary.simpleMessage("एक नई पोस्ट जोड़ें"),
    "addNewBlog" : MessageLookupByLibrary.simpleMessage("नया ब्लॉग जोड़ें"),
    "addToCart" : MessageLookupByLibrary.simpleMessage("कार्ट में डालें"),
    "addToCartSucessfully" : MessageLookupByLibrary.simpleMessage("आपकी कार्ट में जोड़ दिया गया है"),
    "additionalInformation" : MessageLookupByLibrary.simpleMessage("अतिरिक्त जानकारी"),
    "address" : MessageLookupByLibrary.simpleMessage("पता"),
    "agreeWithPrivacy" : MessageLookupByLibrary.simpleMessage("गोपनीयता और शब्द"),
    "all" : MessageLookupByLibrary.simpleMessage("सब"),
    "apply" : MessageLookupByLibrary.simpleMessage("लागू करें"),
    "arabic" : MessageLookupByLibrary.simpleMessage("अरबी"),
    "attributes" : MessageLookupByLibrary.simpleMessage("गुण"),
    "availability" : MessageLookupByLibrary.simpleMessage("उपलब्धता"),
    "backToShop" : MessageLookupByLibrary.simpleMessage("वापस दुकान में"),
    "bagsCollections" : MessageLookupByLibrary.simpleMessage("गियर्स कलेक्शन"),
    "blog" : MessageLookupByLibrary.simpleMessage("ब्लॉग"),
    "brazil" : MessageLookupByLibrary.simpleMessage("ब्राज़िल"),
    "buyNow" : MessageLookupByLibrary.simpleMessage("अभी खरीदें"),
    "byCategory" : MessageLookupByLibrary.simpleMessage("श्रेणी के द्वारा"),
    "byPrice" : MessageLookupByLibrary.simpleMessage("मूल्य द्वारा"),
    "cancel" : MessageLookupByLibrary.simpleMessage("रद्द करना"),
    "cart" : MessageLookupByLibrary.simpleMessage("गाड़ी"),
    "categories" : MessageLookupByLibrary.simpleMessage("श्रेणियाँ"),
    "category" : MessageLookupByLibrary.simpleMessage("वर्ग"),
    "checkout" : MessageLookupByLibrary.simpleMessage("चेक आउट"),
    "chinese" : MessageLookupByLibrary.simpleMessage("चीनी"),
    "chooseYourPaymentMethod" : MessageLookupByLibrary.simpleMessage("अपनी भुगतान विधि चुनें"),
    "city" : MessageLookupByLibrary.simpleMessage("शहर"),
    "cityIsRequired" : MessageLookupByLibrary.simpleMessage("शहर के मैदान की आवश्यकता है"),
    "clear" : MessageLookupByLibrary.simpleMessage("स्पष्ट"),
    "clearCart" : MessageLookupByLibrary.simpleMessage("कार्ट को खाली करें"),
    "close" : MessageLookupByLibrary.simpleMessage("बंद करे"),
    "color" : MessageLookupByLibrary.simpleMessage("रंग"),
    "commentFirst" : MessageLookupByLibrary.simpleMessage("कृपया अपनी टिप्पणी लिखें"),
    "commentSuccessfully" : MessageLookupByLibrary.simpleMessage("अपनी टिप्पणी स्वीकृत होने तक प्रतीक्षा करें, कृपया सफलतापूर्वक टिप्पणी करें"),
    "contact" : MessageLookupByLibrary.simpleMessage("संपर्क करें"),
    "content" : MessageLookupByLibrary.simpleMessage("सामग्री"),
    "continueToPayment" : MessageLookupByLibrary.simpleMessage("भुगतान जारी रखें"),
    "continueToReview" : MessageLookupByLibrary.simpleMessage("समीक्षा जारी रखें"),
    "continueToShipping" : MessageLookupByLibrary.simpleMessage("शिपिंग के लिए जारी रखें"),
    "country" : MessageLookupByLibrary.simpleMessage("देश"),
    "countryIsRequired" : MessageLookupByLibrary.simpleMessage("देश के क्षेत्र की आवश्यकता है"),
    "couponCode" : MessageLookupByLibrary.simpleMessage("कूपन कोड"),
    "couponMsgSuccess" : MessageLookupByLibrary.simpleMessage("बधाई हो! कूपन कोड सफलतापूर्वक लागू किया गया"),
    "createAnAccount" : MessageLookupByLibrary.simpleMessage("खाता बनाएं"),
    "createNewPostSuccessfully" : MessageLookupByLibrary.simpleMessage("आपका पोस्ट सफलतापूर्वक एक मसौदे के रूप में बनाया गया है। कृपया अपने व्यवस्थापक साइट पर एक नज़र डालें।"),
    "createPost" : MessageLookupByLibrary.simpleMessage(""),
    "currencies" : MessageLookupByLibrary.simpleMessage("मुद्राओं"),
    "currentPassword" : MessageLookupByLibrary.simpleMessage("वर्तमान पासवर्ड"),
    "darkTheme" : MessageLookupByLibrary.simpleMessage("डार्क थीम"),
    "date" : MessageLookupByLibrary.simpleMessage("दिनांक"),
    "daysAgo" : m0,
    "description" : MessageLookupByLibrary.simpleMessage("विवरण"),
    "discount" : MessageLookupByLibrary.simpleMessage("छूट"),
    "displayName" : MessageLookupByLibrary.simpleMessage("प्रदर्शित होने वाला नाम"),
    "done" : MessageLookupByLibrary.simpleMessage("किया हुआ"),
    "dontHaveAccount" : MessageLookupByLibrary.simpleMessage("खाता नहीं है?"),
    "dutch" : MessageLookupByLibrary.simpleMessage("डच"),
    "email" : MessageLookupByLibrary.simpleMessage("ईमेल"),
    "emailIsRequired" : MessageLookupByLibrary.simpleMessage("ईमेल क्षेत्र की आवश्यकता है"),
    "emptyCartSubtitle" : MessageLookupByLibrary.simpleMessage("ऐसा लगता है कि आपने अभी तक किसी आइटम को बैग में जोड़ा नहीं है। इसे भरने के लिए खरीदारी शुरू करें।"),
    "emptyComment" : MessageLookupByLibrary.simpleMessage("आपकी टिप्पणी खाली नहीं हो सकती"),
    "emptySearch" : MessageLookupByLibrary.simpleMessage("आपने अभी तक आइटम नहीं खोजे हैं। चलिए अब शुरू करते हैं - हम आपकी मदद करेंगे।"),
    "emptyWishlistSubtitle" : MessageLookupByLibrary.simpleMessage("किसी उत्पाद के आगे किसी भी दिल को फ़ावोटाइट पर टैप करें। हम उन्हें आपके लिए यहाँ सहेजेंगे!"),
    "english" : MessageLookupByLibrary.simpleMessage("अंग्रेज़ी"),
    "enterYourEmail" : MessageLookupByLibrary.simpleMessage("अपना ईमेल दर्ज करें"),
    "enterYourPassword" : MessageLookupByLibrary.simpleMessage("अपना पासवर्ड डालें"),
    "events" : MessageLookupByLibrary.simpleMessage("आयोजन"),
    "featureProducts" : MessageLookupByLibrary.simpleMessage("फ़ीचर उत्पाद"),
    "filter" : MessageLookupByLibrary.simpleMessage("फिल्टर"),
    "firstName" : MessageLookupByLibrary.simpleMessage("पहला नाम"),
    "firstNameIsRequired" : MessageLookupByLibrary.simpleMessage("पहले नाम फ़ील्ड की आवश्यकता है"),
    "french" : MessageLookupByLibrary.simpleMessage("फ्रेंच"),
    "generalSetting" : MessageLookupByLibrary.simpleMessage("सामान्य सेटिंग"),
    "german" : MessageLookupByLibrary.simpleMessage("जर्मन"),
    "getNotification" : MessageLookupByLibrary.simpleMessage("अधिसूचना प्राप्त"),
    "goBackToAddress" : MessageLookupByLibrary.simpleMessage("पता करने के लिए वापस जाएँ"),
    "goBackToReview" : MessageLookupByLibrary.simpleMessage("समीक्षा करने के लिए वापस जाएं"),
    "goBackToShipping" : MessageLookupByLibrary.simpleMessage("शिपिंग पर वापस जाएं"),
    "hebrew" : MessageLookupByLibrary.simpleMessage("यहूदी"),
    "hindi" : MessageLookupByLibrary.simpleMessage("हिंदी"),
    "home" : MessageLookupByLibrary.simpleMessage("होम"),
    "hoursAgo" : m1,
    "hungary" : MessageLookupByLibrary.simpleMessage("हंगरी"),
    "iAgree" : MessageLookupByLibrary.simpleMessage("मैं सहमत हूँ"),
    "imageFeature" : MessageLookupByLibrary.simpleMessage("छवि सुविधा"),
    "inStock" : MessageLookupByLibrary.simpleMessage("स्टॉक में"),
    "indonesian" : MessageLookupByLibrary.simpleMessage("इन्डोनेशियाई"),
    "invalidSMSCode" : MessageLookupByLibrary.simpleMessage("अमान्य एसएमएस सत्यापन कोड"),
    "italian" : MessageLookupByLibrary.simpleMessage("इतालवी"),
    "items" : MessageLookupByLibrary.simpleMessage("आइटम"),
    "itsOrdered" : MessageLookupByLibrary.simpleMessage("यह आदेश दिया है!"),
    "iwantToCreateAccount" : MessageLookupByLibrary.simpleMessage("मैं एक खाता बनाना चाहता हूं"),
    "japanese" : MessageLookupByLibrary.simpleMessage("जापानी"),
    "korean" : MessageLookupByLibrary.simpleMessage("कोरियाई"),
    "language" : MessageLookupByLibrary.simpleMessage("भाषा"),
    "languageSuccess" : MessageLookupByLibrary.simpleMessage("भाषा को सफलतापूर्वक अपडेट किया गया है"),
    "lastName" : MessageLookupByLibrary.simpleMessage("अंतिम नाम"),
    "lastNameIsRequired" : MessageLookupByLibrary.simpleMessage("अंतिम नाम फ़ील्ड आवश्यक है"),
    "layout" : MessageLookupByLibrary.simpleMessage("लेआउट"),
    "listMessages" : MessageLookupByLibrary.simpleMessage("संदेश सूचित करें"),
    "loadFail" : MessageLookupByLibrary.simpleMessage("लोड विफल हो गया!"),
    "loading" : MessageLookupByLibrary.simpleMessage("लोड हो रहा है..."),
    "login" : MessageLookupByLibrary.simpleMessage("लॉग इन करें"),
    "loginToComment" : MessageLookupByLibrary.simpleMessage("कृपया टिप्पणी में लॉगिन करें"),
    "loginToYourAccount" : MessageLookupByLibrary.simpleMessage("अपने खाते में प्रवेश करें"),
    "logout" : MessageLookupByLibrary.simpleMessage("लोग आउट"),
    "manCollections" : MessageLookupByLibrary.simpleMessage("मैन कलेक्शंस"),
    "minutesAgo" : m2,
    "momentAgo" : MessageLookupByLibrary.simpleMessage("एक क्षण पहले"),
    "monthsAgo" : m3,
    "myCart" : MessageLookupByLibrary.simpleMessage("मेरी गाड़ी"),
    "myPoints" : MessageLookupByLibrary.simpleMessage("मेरे अंक"),
    "myWishList" : MessageLookupByLibrary.simpleMessage("मेरी इच्छा सूची"),
    "newPassword" : MessageLookupByLibrary.simpleMessage("नया पासवर्ड"),
    "next" : MessageLookupByLibrary.simpleMessage("आगामी"),
    "niceName" : MessageLookupByLibrary.simpleMessage("अच्छा नाम"),
    "noData" : MessageLookupByLibrary.simpleMessage("कोई और अधिक डेटा"),
    "noFavoritesYet" : MessageLookupByLibrary.simpleMessage("अभी तक कोई पसंदीदा नहीं।"),
    "noInternetConnection" : MessageLookupByLibrary.simpleMessage("कोई इंटरनेट कनेक्शन नहीं"),
    "noOrders" : MessageLookupByLibrary.simpleMessage("कोई आदेश नहीं"),
    "noPost" : MessageLookupByLibrary.simpleMessage("आपने अभी तक कोई ब्लॉग नहीं बनाया है!"),
    "noProduct" : MessageLookupByLibrary.simpleMessage("कोई उत्पाद नहीं"),
    "noReviews" : MessageLookupByLibrary.simpleMessage("कोई समीक्षा नहीं"),
    "or" : MessageLookupByLibrary.simpleMessage("या"),
    "orderDate" : MessageLookupByLibrary.simpleMessage("आदेश की तारीख"),
    "orderDetail" : MessageLookupByLibrary.simpleMessage("ऑर्डर का विवरण"),
    "orderHistory" : MessageLookupByLibrary.simpleMessage("आदेश इतिहास"),
    "orderNo" : MessageLookupByLibrary.simpleMessage("आदेश संख्या।"),
    "orderNotes" : MessageLookupByLibrary.simpleMessage("ऑर्डर नोट"),
    "orderSuccessMsg1" : MessageLookupByLibrary.simpleMessage("आप हमारे वितरण स्थिति सुविधा का उपयोग करके अपने आदेश की स्थिति की जांच कर सकते हैं। आपको अपने आदेश के विवरण और इसकी प्रगति को ट्रैक करने के लिए लिंक के साथ एक आदेश पुष्टिकरण ई-मेल प्राप्त होगा।"),
    "orderSuccessMsg2" : MessageLookupByLibrary.simpleMessage("आप पहले से परिभाषित ई-मेल और पासवर्ड का उपयोग करके अपने खाते में लॉग इन कर सकते हैं। अपने खाते पर आप अपने प्रोफ़ाइल डेटा को संपादित कर सकते हैं, लेनदेन के इतिहास की जांच कर सकते हैं, न्यूज़लेटर की सदस्यता को संपादित कर सकते हैं।"),
    "orderSuccessTitle1" : MessageLookupByLibrary.simpleMessage("आपने सफलतापूर्वक आदेश दिया है"),
    "orderSuccessTitle2" : MessageLookupByLibrary.simpleMessage("आपका खाता"),
    "outOfStock" : MessageLookupByLibrary.simpleMessage("स्टॉक ख़त्म"),
    "pageView" : MessageLookupByLibrary.simpleMessage("पृष्ठ का दृश्य"),
    "password" : MessageLookupByLibrary.simpleMessage("पारण शब्द"),
    "payment" : MessageLookupByLibrary.simpleMessage("भुगतान"),
    "paymentMethod" : MessageLookupByLibrary.simpleMessage("भुगतान का तरीका"),
    "paymentMethods" : MessageLookupByLibrary.simpleMessage("भुगतान की विधि"),
    "phoneIsRequired" : MessageLookupByLibrary.simpleMessage("फ़ोन नंबर फ़ील्ड आवश्यक है"),
    "phoneNumber" : MessageLookupByLibrary.simpleMessage("फ़ोन नंबर"),
    "placeMyOrder" : MessageLookupByLibrary.simpleMessage("मेरा ऑर्डर लीजिये"),
    "pleaseInput" : MessageLookupByLibrary.simpleMessage("कृपया सभी क्षेत्रों में इनपुट भरें"),
    "point" : MessageLookupByLibrary.simpleMessage("बिंदु"),
    "postManagement" : MessageLookupByLibrary.simpleMessage("पोस्ट प्रबंधन"),
    "privacyAndTerm" : MessageLookupByLibrary.simpleMessage("गोपनीयता और शब्द"),
    "privacyPolicy" : MessageLookupByLibrary.simpleMessage("गोपनीयता नीति"),
    "productAdded" : MessageLookupByLibrary.simpleMessage("उत्पाद जोड़ा जाता है"),
    "productRating" : MessageLookupByLibrary.simpleMessage("तुम्हारी रेटिंग"),
    "products" : MessageLookupByLibrary.simpleMessage("उत्पाद"),
    "pullToLoadMore" : MessageLookupByLibrary.simpleMessage("अधिक लोड करने के लिए खींचो"),
    "rateTheApp" : MessageLookupByLibrary.simpleMessage("एप्लिकेशन की श्रेणी बताओ"),
    "ratingFirst" : MessageLookupByLibrary.simpleMessage("कृपया अपनी टिप्पणी भेजने से पहले रेटिंग दें"),
    "readReviews" : MessageLookupByLibrary.simpleMessage("समीक्षा"),
    "recentSearches" : MessageLookupByLibrary.simpleMessage("इतिहास"),
    "recentView" : MessageLookupByLibrary.simpleMessage("आपका हाल ही का दृश्य"),
    "recents" : MessageLookupByLibrary.simpleMessage("हाल का"),
    "refundRequest" : MessageLookupByLibrary.simpleMessage("धनवापसी का अनुरोध"),
    "releaseToLoadMore" : MessageLookupByLibrary.simpleMessage("अधिक लोड करने के लिए रिलीज़ करें"),
    "remove" : MessageLookupByLibrary.simpleMessage("हटाना"),
    "reset" : MessageLookupByLibrary.simpleMessage("रीसेट"),
    "review" : MessageLookupByLibrary.simpleMessage("समीक्षा"),
    "romanian" : MessageLookupByLibrary.simpleMessage("रोमानियाई"),
    "russian" : MessageLookupByLibrary.simpleMessage("रूसी"),
    "sale" : m4,
    "saveAddress" : MessageLookupByLibrary.simpleMessage("पता सहेजें"),
    "saveAddressSuccess" : MessageLookupByLibrary.simpleMessage("आपका पता आपके स्थानीय में मौजूद है"),
    "saveToWishList" : MessageLookupByLibrary.simpleMessage("विशलिस्ट में सहेजें"),
    "search" : MessageLookupByLibrary.simpleMessage("खोज"),
    "searchForItems" : MessageLookupByLibrary.simpleMessage("आइटम के लिए खोजें"),
    "searchInput" : MessageLookupByLibrary.simpleMessage("कृपया खोज क्षेत्र में इनपुट लिखें"),
    "searchingAddress" : MessageLookupByLibrary.simpleMessage("खोज का पता"),
    "secondsAgo" : m5,
    "seeAll" : MessageLookupByLibrary.simpleMessage("सभी देखें"),
    "selectAddress" : MessageLookupByLibrary.simpleMessage("पता चुनें"),
    "selectTheColor" : MessageLookupByLibrary.simpleMessage("रंग का चयन करें"),
    "selectTheQuantity" : MessageLookupByLibrary.simpleMessage("मात्रा का चयन करें"),
    "selectTheSize" : MessageLookupByLibrary.simpleMessage("आकार का चयन करें"),
    "sendSMSCode" : MessageLookupByLibrary.simpleMessage("कोड प्राप्त करें"),
    "settings" : MessageLookupByLibrary.simpleMessage("सेटिंग्स"),
    "share" : MessageLookupByLibrary.simpleMessage("शेयर"),
    "shipping" : MessageLookupByLibrary.simpleMessage("शिपिंग"),
    "shippingAddress" : MessageLookupByLibrary.simpleMessage("शिपिंग पता"),
    "shippingMethod" : MessageLookupByLibrary.simpleMessage("शिपिंग का तरीका"),
    "shop" : MessageLookupByLibrary.simpleMessage("दुकान"),
    "showAllMyOrdered" : MessageLookupByLibrary.simpleMessage("मेरे सभी आदेश दिखाए"),
    "showGallery" : MessageLookupByLibrary.simpleMessage("गैलरी दिखाएं"),
    "signIn" : MessageLookupByLibrary.simpleMessage("दाखिल करना"),
    "signInWithEmail" : MessageLookupByLibrary.simpleMessage("ईमेल से साइन इन करें"),
    "signUp" : MessageLookupByLibrary.simpleMessage("साइन अप करें"),
    "signup" : MessageLookupByLibrary.simpleMessage("साइन अप करें"),
    "size" : MessageLookupByLibrary.simpleMessage("आकार"),
    "spanish" : MessageLookupByLibrary.simpleMessage("स्पेनिश"),
    "startExploring" : MessageLookupByLibrary.simpleMessage("खोज शुरू करें"),
    "startShopping" : MessageLookupByLibrary.simpleMessage("खरीदारी शुरू करें"),
    "stateIsRequired" : MessageLookupByLibrary.simpleMessage("राज्य क्षेत्र की आवश्यकता है"),
    "stateProvince" : MessageLookupByLibrary.simpleMessage("राज्य / प्रांत"),
    "status" : MessageLookupByLibrary.simpleMessage("स्थिति"),
    "streetIsRequired" : MessageLookupByLibrary.simpleMessage("सड़क का नाम फ़ील्ड आवश्यक है"),
    "streetName" : MessageLookupByLibrary.simpleMessage("सड़क का नाम"),
    "submit" : MessageLookupByLibrary.simpleMessage("जमा करें"),
    "subtotal" : MessageLookupByLibrary.simpleMessage("उप-योग"),
    "tags" : MessageLookupByLibrary.simpleMessage("टैग"),
    "thailand" : MessageLookupByLibrary.simpleMessage("थाईलैंड"),
    "title" : MessageLookupByLibrary.simpleMessage("शीर्षक"),
    "total" : MessageLookupByLibrary.simpleMessage("संपूर्ण"),
    "totalTax" : MessageLookupByLibrary.simpleMessage("कुल कर"),
    "trackingNumberIs" : MessageLookupByLibrary.simpleMessage("ट्रैकिंग नंबर है"),
    "trackingPage" : MessageLookupByLibrary.simpleMessage("ट्रैकिंग पृष्ठ"),
    "turkish" : MessageLookupByLibrary.simpleMessage("तुर्की"),
    "unavailable" : MessageLookupByLibrary.simpleMessage("अनुपलब्ध"),
    "update" : MessageLookupByLibrary.simpleMessage("अद्यतन करें"),
    "updateUserInfor" : MessageLookupByLibrary.simpleMessage("प्रोफ़ाइल अपडेट करें"),
    "username" : MessageLookupByLibrary.simpleMessage("उपयोगकर्ता नाम"),
    "verifySMSCode" : MessageLookupByLibrary.simpleMessage("सत्यापित करें"),
    "video" : MessageLookupByLibrary.simpleMessage("वीडियो"),
    "vietnamese" : MessageLookupByLibrary.simpleMessage("वियतनाम"),
    "weFoundBlogs" : MessageLookupByLibrary.simpleMessage("हमें ब्लॉग मिला"),
    "weFoundProducts" : m6,
    "webView" : MessageLookupByLibrary.simpleMessage("वेब दृश्य"),
    "welcome" : MessageLookupByLibrary.simpleMessage("स्वागत हे"),
    "womanCollections" : MessageLookupByLibrary.simpleMessage("स्त्री संग्रह"),
    "writeComment" : MessageLookupByLibrary.simpleMessage("अपनी टिप्पणी लिखें"),
    "writeYourNote" : MessageLookupByLibrary.simpleMessage("अपना नोट लिखें"),
    "yearsAgo" : m7,
    "youHavePoints" : MessageLookupByLibrary.simpleMessage("आपके पास \$ पॉइंट पॉइंट हैं"),
    "youMightAlsoLike" : MessageLookupByLibrary.simpleMessage("शयद आपको भी ये अच्छा लगे"),
    "yourBagIsEmpty" : MessageLookupByLibrary.simpleMessage("आपका बैग खाली है"),
    "yourNote" : MessageLookupByLibrary.simpleMessage("आपका नोट"),
    "zipCode" : MessageLookupByLibrary.simpleMessage("पिन कोड"),
    "zipCodeIsRequired" : MessageLookupByLibrary.simpleMessage("ज़िप कोड फ़ील्ड आवश्यक है")
  };
}
