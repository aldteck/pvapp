// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a it locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'it';

  static m0(day) => "${day} giorni fa";

  static m1(hour) => "${hour} ore fa";

  static m2(minute) => "${minute} minuti fa";

  static m3(month) => "${month} mesi fa";

  static m4(percent) => "Vendita ${percent} %";

  static m5(second) => "${second} secondi fa";

  static m6(length) => "Abbiamo trovato ${length} prodotti";

  static m7(year) => "${year} anni fa";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "aboutUs" : MessageLookupByLibrary.simpleMessage("Riguardo a noi"),
    "addANewPost" : MessageLookupByLibrary.simpleMessage("Aggiungi un nuovo post"),
    "addNewBlog" : MessageLookupByLibrary.simpleMessage("Aggiungi nuovo blog"),
    "addToCart" : MessageLookupByLibrary.simpleMessage("Aggiungi al carrello"),
    "addToCartSucessfully" : MessageLookupByLibrary.simpleMessage("sono stati aggiunti al tuo carrello"),
    "additionalInformation" : MessageLookupByLibrary.simpleMessage("Informazioni aggiuntive"),
    "address" : MessageLookupByLibrary.simpleMessage("Indirizzo"),
    "agreeWithPrivacy" : MessageLookupByLibrary.simpleMessage("Privacy e durata"),
    "all" : MessageLookupByLibrary.simpleMessage("Tutti"),
    "apply" : MessageLookupByLibrary.simpleMessage("applicare"),
    "arabic" : MessageLookupByLibrary.simpleMessage("arabo"),
    "attributes" : MessageLookupByLibrary.simpleMessage("attributi"),
    "availability" : MessageLookupByLibrary.simpleMessage("Disponibilità"),
    "backToShop" : MessageLookupByLibrary.simpleMessage("Torna al negozio"),
    "bagsCollections" : MessageLookupByLibrary.simpleMessage("Collezioni di ingranaggi"),
    "blog" : MessageLookupByLibrary.simpleMessage("blog"),
    "brazil" : MessageLookupByLibrary.simpleMessage("Brasile"),
    "buyNow" : MessageLookupByLibrary.simpleMessage("Acquista ora"),
    "byCategory" : MessageLookupByLibrary.simpleMessage("Per categoria"),
    "byPrice" : MessageLookupByLibrary.simpleMessage("Per prezzo"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Annulla"),
    "cart" : MessageLookupByLibrary.simpleMessage("Carrello"),
    "categories" : MessageLookupByLibrary.simpleMessage("categorie"),
    "category" : MessageLookupByLibrary.simpleMessage("CATEGORIA"),
    "checkout" : MessageLookupByLibrary.simpleMessage("Check-out"),
    "chinese" : MessageLookupByLibrary.simpleMessage("cinese"),
    "chooseYourPaymentMethod" : MessageLookupByLibrary.simpleMessage("Scegli il tuo metodo di pagamento"),
    "city" : MessageLookupByLibrary.simpleMessage("città"),
    "cityIsRequired" : MessageLookupByLibrary.simpleMessage("Il campo della città è obbligatorio"),
    "clear" : MessageLookupByLibrary.simpleMessage("Chiaro"),
    "clearCart" : MessageLookupByLibrary.simpleMessage("Cancella il carrello"),
    "close" : MessageLookupByLibrary.simpleMessage("Vicino"),
    "color" : MessageLookupByLibrary.simpleMessage("Colore"),
    "commentFirst" : MessageLookupByLibrary.simpleMessage("Per favore scrivi il tuo commento"),
    "commentSuccessfully" : MessageLookupByLibrary.simpleMessage("Commenta con successo, attendi fino all\'approvazione del tuo commento"),
    "contact" : MessageLookupByLibrary.simpleMessage("Contatto"),
    "content" : MessageLookupByLibrary.simpleMessage("Soddisfare"),
    "continueToPayment" : MessageLookupByLibrary.simpleMessage("Continua al pagamento"),
    "continueToReview" : MessageLookupByLibrary.simpleMessage("Continua alla revisione"),
    "continueToShipping" : MessageLookupByLibrary.simpleMessage("Continua alla spedizione"),
    "country" : MessageLookupByLibrary.simpleMessage("Nazione"),
    "countryIsRequired" : MessageLookupByLibrary.simpleMessage("Il campo del paese è obbligatorio"),
    "couponCode" : MessageLookupByLibrary.simpleMessage("Codice coupon"),
    "couponMsgSuccess" : MessageLookupByLibrary.simpleMessage("Congratulazioni! Codice coupon applicato con successo"),
    "createAnAccount" : MessageLookupByLibrary.simpleMessage("Crea un account"),
    "createNewPostSuccessfully" : MessageLookupByLibrary.simpleMessage("Il tuo post è stato creato correttamente come bozza. Dai un\'occhiata al tuo sito di amministrazione."),
    "createPost" : MessageLookupByLibrary.simpleMessage(""),
    "currencies" : MessageLookupByLibrary.simpleMessage("valute"),
    "currentPassword" : MessageLookupByLibrary.simpleMessage("Password attuale"),
    "darkTheme" : MessageLookupByLibrary.simpleMessage("Tema scuro"),
    "date" : MessageLookupByLibrary.simpleMessage("Data"),
    "daysAgo" : m0,
    "description" : MessageLookupByLibrary.simpleMessage("Descrizione"),
    "discount" : MessageLookupByLibrary.simpleMessage("sconto"),
    "displayName" : MessageLookupByLibrary.simpleMessage("Nome da visualizzare"),
    "done" : MessageLookupByLibrary.simpleMessage("Fatto"),
    "dontHaveAccount" : MessageLookupByLibrary.simpleMessage("Non hai un account?"),
    "dutch" : MessageLookupByLibrary.simpleMessage("olandese"),
    "email" : MessageLookupByLibrary.simpleMessage("E-mail"),
    "emailIsRequired" : MessageLookupByLibrary.simpleMessage("Il campo Email è obbligatorio"),
    "emptyCartSubtitle" : MessageLookupByLibrary.simpleMessage("Sembra che tu non abbia ancora aggiunto alcun articolo alla borsa. Inizia a fare acquisti per compilarlo."),
    "emptyComment" : MessageLookupByLibrary.simpleMessage("Il tuo commento non può essere vuoto"),
    "emptySearch" : MessageLookupByLibrary.simpleMessage("Non hai ancora cercato elementi. Cominciamo ora: ti aiuteremo."),
    "emptyWishlistSubtitle" : MessageLookupByLibrary.simpleMessage("Tocca un cuore qualsiasi accanto a un prodotto per creare un preferito. Li salveremo per te qui!"),
    "english" : MessageLookupByLibrary.simpleMessage("Inglese"),
    "enterYourEmail" : MessageLookupByLibrary.simpleMessage("Inserisci il tuo indirizzo email"),
    "enterYourPassword" : MessageLookupByLibrary.simpleMessage("Inserisci la tua password"),
    "events" : MessageLookupByLibrary.simpleMessage("eventi"),
    "featureProducts" : MessageLookupByLibrary.simpleMessage("Prodotti caratteristici"),
    "filter" : MessageLookupByLibrary.simpleMessage("Filtro"),
    "firstName" : MessageLookupByLibrary.simpleMessage("Nome di battesimo"),
    "firstNameIsRequired" : MessageLookupByLibrary.simpleMessage("Il campo del nome è obbligatorio"),
    "french" : MessageLookupByLibrary.simpleMessage("Francese"),
    "generalSetting" : MessageLookupByLibrary.simpleMessage("impostazioni generali"),
    "german" : MessageLookupByLibrary.simpleMessage("Tedesco"),
    "getNotification" : MessageLookupByLibrary.simpleMessage("Ottenere la notifica"),
    "goBackToAddress" : MessageLookupByLibrary.simpleMessage("Torna all\'indirizzo"),
    "goBackToReview" : MessageLookupByLibrary.simpleMessage("Torna alla recensione"),
    "goBackToShipping" : MessageLookupByLibrary.simpleMessage("Torna alla spedizione"),
    "hebrew" : MessageLookupByLibrary.simpleMessage("ebraico"),
    "hindi" : MessageLookupByLibrary.simpleMessage("hindi"),
    "home" : MessageLookupByLibrary.simpleMessage("Casa"),
    "hoursAgo" : m1,
    "hungary" : MessageLookupByLibrary.simpleMessage("Ungheria"),
    "iAgree" : MessageLookupByLibrary.simpleMessage("Sono d\'accordo con"),
    "imageFeature" : MessageLookupByLibrary.simpleMessage("Caratteristica dell\'immagine"),
    "inStock" : MessageLookupByLibrary.simpleMessage("disponibile"),
    "indonesian" : MessageLookupByLibrary.simpleMessage("indonesiano"),
    "invalidSMSCode" : MessageLookupByLibrary.simpleMessage("Codice di verifica SMS non valido"),
    "italian" : MessageLookupByLibrary.simpleMessage("Italiano"),
    "items" : MessageLookupByLibrary.simpleMessage("Elementi"),
    "itsOrdered" : MessageLookupByLibrary.simpleMessage("È ordinato!"),
    "iwantToCreateAccount" : MessageLookupByLibrary.simpleMessage("Voglio creare un account"),
    "japanese" : MessageLookupByLibrary.simpleMessage("giapponese"),
    "korean" : MessageLookupByLibrary.simpleMessage("coreano"),
    "language" : MessageLookupByLibrary.simpleMessage("linguaggio"),
    "languageSuccess" : MessageLookupByLibrary.simpleMessage("La lingua è stata aggiornata correttamente"),
    "lastName" : MessageLookupByLibrary.simpleMessage("Cognome"),
    "lastNameIsRequired" : MessageLookupByLibrary.simpleMessage("Il campo del cognome è obbligatorio"),
    "layout" : MessageLookupByLibrary.simpleMessage("Layout"),
    "listMessages" : MessageLookupByLibrary.simpleMessage("Notifica messaggi"),
    "loadFail" : MessageLookupByLibrary.simpleMessage("Caricamento non riuscito!"),
    "loading" : MessageLookupByLibrary.simpleMessage("Caricamento in corso..."),
    "login" : MessageLookupByLibrary.simpleMessage("Accesso"),
    "loginToComment" : MessageLookupByLibrary.simpleMessage("Effettua il login per commentare"),
    "loginToYourAccount" : MessageLookupByLibrary.simpleMessage("ACCEDI AL TUO ACCOUNT"),
    "logout" : MessageLookupByLibrary.simpleMessage("Disconnettersi"),
    "manCollections" : MessageLookupByLibrary.simpleMessage("Collezioni uomo"),
    "minutesAgo" : m2,
    "momentAgo" : MessageLookupByLibrary.simpleMessage("un attimo fa"),
    "monthsAgo" : m3,
    "myCart" : MessageLookupByLibrary.simpleMessage("La mia carta"),
    "myPoints" : MessageLookupByLibrary.simpleMessage("I miei punti"),
    "myWishList" : MessageLookupByLibrary.simpleMessage("La mia lista dei desideri"),
    "newPassword" : MessageLookupByLibrary.simpleMessage("Nuova password"),
    "next" : MessageLookupByLibrary.simpleMessage("Il prossimo"),
    "niceName" : MessageLookupByLibrary.simpleMessage("Bel nome"),
    "noData" : MessageLookupByLibrary.simpleMessage("Nessun altro dato"),
    "noFavoritesYet" : MessageLookupByLibrary.simpleMessage("Nessun preferito ancora."),
    "noInternetConnection" : MessageLookupByLibrary.simpleMessage("Nessuna connessione internet"),
    "noOrders" : MessageLookupByLibrary.simpleMessage("Nessun ordine"),
    "noPost" : MessageLookupByLibrary.simpleMessage("Non hai ancora creato nessun blog!"),
    "noProduct" : MessageLookupByLibrary.simpleMessage("Nessun prodotto"),
    "noReviews" : MessageLookupByLibrary.simpleMessage("Nessuna recensione"),
    "or" : MessageLookupByLibrary.simpleMessage("o"),
    "orderDate" : MessageLookupByLibrary.simpleMessage("Data dell\'ordine"),
    "orderDetail" : MessageLookupByLibrary.simpleMessage("Dettagli dell\'ordine"),
    "orderHistory" : MessageLookupByLibrary.simpleMessage("Cronologia ordini"),
    "orderNo" : MessageLookupByLibrary.simpleMessage("Numero d\'ordine."),
    "orderNotes" : MessageLookupByLibrary.simpleMessage("Note sugli ordini"),
    "orderSuccessMsg1" : MessageLookupByLibrary.simpleMessage("Puoi controllare lo stato del tuo ordine utilizzando la nostra funzione di stato della consegna. Riceverai un\'e-mail di conferma dell\'ordine con i dettagli del tuo ordine e un link per monitorarne lo stato di avanzamento."),
    "orderSuccessMsg2" : MessageLookupByLibrary.simpleMessage("Puoi accedere al tuo account utilizzando e-mail e password definite in precedenza. Sul tuo account puoi modificare i dati del tuo profilo, controllare la cronologia delle transazioni, modificare l\'iscrizione alla newsletter."),
    "orderSuccessTitle1" : MessageLookupByLibrary.simpleMessage("Hai effettuato l\'ordine con successo"),
    "orderSuccessTitle2" : MessageLookupByLibrary.simpleMessage("Il tuo account"),
    "outOfStock" : MessageLookupByLibrary.simpleMessage("Esaurito"),
    "pageView" : MessageLookupByLibrary.simpleMessage("Visualizzazione della pagina"),
    "password" : MessageLookupByLibrary.simpleMessage("Parola d\'ordine"),
    "payment" : MessageLookupByLibrary.simpleMessage("Pagamento"),
    "paymentMethod" : MessageLookupByLibrary.simpleMessage("Metodo di pagamento"),
    "paymentMethods" : MessageLookupByLibrary.simpleMessage("modalità di pagamento"),
    "phoneIsRequired" : MessageLookupByLibrary.simpleMessage("Il campo del numero di telefono è obbligatorio"),
    "phoneNumber" : MessageLookupByLibrary.simpleMessage("Numero di telefono"),
    "placeMyOrder" : MessageLookupByLibrary.simpleMessage("Effettua il mio ordine"),
    "pleaseInput" : MessageLookupByLibrary.simpleMessage("Si prega di inserire compilare tutti i campi"),
    "point" : MessageLookupByLibrary.simpleMessage("Punto"),
    "postManagement" : MessageLookupByLibrary.simpleMessage("Post Management"),
    "privacyAndTerm" : MessageLookupByLibrary.simpleMessage("Privacy e durata"),
    "privacyPolicy" : MessageLookupByLibrary.simpleMessage("POLITICA SULLA RISERVATEZZA"),
    "productAdded" : MessageLookupByLibrary.simpleMessage("Il prodotto viene aggiunto"),
    "productRating" : MessageLookupByLibrary.simpleMessage("Il tuo punteggio"),
    "products" : MessageLookupByLibrary.simpleMessage("prodotti"),
    "pullToLoadMore" : MessageLookupByLibrary.simpleMessage("Tirare per caricare di più"),
    "rateTheApp" : MessageLookupByLibrary.simpleMessage("Valuta l\'app"),
    "ratingFirst" : MessageLookupByLibrary.simpleMessage("Si prega di valutare prima di inviare il commento"),
    "readReviews" : MessageLookupByLibrary.simpleMessage("recensioni"),
    "recentSearches" : MessageLookupByLibrary.simpleMessage("storia"),
    "recentView" : MessageLookupByLibrary.simpleMessage("La tua vista recente"),
    "recents" : MessageLookupByLibrary.simpleMessage("recente"),
    "refundRequest" : MessageLookupByLibrary.simpleMessage("Richiesta di rimborso"),
    "releaseToLoadMore" : MessageLookupByLibrary.simpleMessage("Rilascia per caricare di più"),
    "remove" : MessageLookupByLibrary.simpleMessage("Rimuovere"),
    "reset" : MessageLookupByLibrary.simpleMessage("Reset"),
    "review" : MessageLookupByLibrary.simpleMessage("Revisione"),
    "romanian" : MessageLookupByLibrary.simpleMessage("rumeno"),
    "russian" : MessageLookupByLibrary.simpleMessage("russo"),
    "sale" : m4,
    "saveAddress" : MessageLookupByLibrary.simpleMessage("Salva indirizzo"),
    "saveAddressSuccess" : MessageLookupByLibrary.simpleMessage("Il tuo indirizzo esiste nel tuo locale"),
    "saveToWishList" : MessageLookupByLibrary.simpleMessage("Salva nella lista dei desideri"),
    "search" : MessageLookupByLibrary.simpleMessage("ricerca"),
    "searchForItems" : MessageLookupByLibrary.simpleMessage("Cerca elementi"),
    "searchInput" : MessageLookupByLibrary.simpleMessage("Si prega di scrivere input nel campo di ricerca"),
    "searchingAddress" : MessageLookupByLibrary.simpleMessage("Ricerca indirizzo"),
    "secondsAgo" : m5,
    "seeAll" : MessageLookupByLibrary.simpleMessage("vedi tutto"),
    "selectAddress" : MessageLookupByLibrary.simpleMessage("Seleziona indirizzo"),
    "selectTheColor" : MessageLookupByLibrary.simpleMessage("Seleziona il colore"),
    "selectTheQuantity" : MessageLookupByLibrary.simpleMessage("Seleziona la quantità"),
    "selectTheSize" : MessageLookupByLibrary.simpleMessage("Seleziona la taglia"),
    "sendSMSCode" : MessageLookupByLibrary.simpleMessage("OTTENERE IL CODICE"),
    "settings" : MessageLookupByLibrary.simpleMessage("impostazioni"),
    "share" : MessageLookupByLibrary.simpleMessage("Condividere"),
    "shipping" : MessageLookupByLibrary.simpleMessage("spedizione"),
    "shippingAddress" : MessageLookupByLibrary.simpleMessage("Indirizzo di spedizione"),
    "shippingMethod" : MessageLookupByLibrary.simpleMessage("metodo di spedizione"),
    "shop" : MessageLookupByLibrary.simpleMessage("negozio"),
    "showAllMyOrdered" : MessageLookupByLibrary.simpleMessage("Mostra tutti i miei ordini"),
    "showGallery" : MessageLookupByLibrary.simpleMessage("Mostra galleria"),
    "signIn" : MessageLookupByLibrary.simpleMessage("registrati"),
    "signInWithEmail" : MessageLookupByLibrary.simpleMessage("Accedi con Email"),
    "signUp" : MessageLookupByLibrary.simpleMessage("Iscriviti"),
    "signup" : MessageLookupByLibrary.simpleMessage("Iscriviti"),
    "size" : MessageLookupByLibrary.simpleMessage("Taglia"),
    "spanish" : MessageLookupByLibrary.simpleMessage("Spagnolo"),
    "startExploring" : MessageLookupByLibrary.simpleMessage("Inizia a esplorare"),
    "startShopping" : MessageLookupByLibrary.simpleMessage("Inizia lo shopping"),
    "stateIsRequired" : MessageLookupByLibrary.simpleMessage("Il campo dello stato è obbligatorio"),
    "stateProvince" : MessageLookupByLibrary.simpleMessage("Stato / Provincia"),
    "status" : MessageLookupByLibrary.simpleMessage("stato"),
    "streetIsRequired" : MessageLookupByLibrary.simpleMessage("Il campo del nome della via è obbligatorio"),
    "streetName" : MessageLookupByLibrary.simpleMessage("nome della strada"),
    "submit" : MessageLookupByLibrary.simpleMessage("Sottoscrivi"),
    "subtotal" : MessageLookupByLibrary.simpleMessage("totale parziale"),
    "tags" : MessageLookupByLibrary.simpleMessage("tag"),
    "thailand" : MessageLookupByLibrary.simpleMessage("Tailandia"),
    "title" : MessageLookupByLibrary.simpleMessage("Titolo"),
    "total" : MessageLookupByLibrary.simpleMessage("totale"),
    "totalTax" : MessageLookupByLibrary.simpleMessage("Tasse totali"),
    "trackingNumberIs" : MessageLookupByLibrary.simpleMessage("Il numero di tracciamento è"),
    "trackingPage" : MessageLookupByLibrary.simpleMessage("Pagina di monitoraggio"),
    "turkish" : MessageLookupByLibrary.simpleMessage("turco"),
    "unavailable" : MessageLookupByLibrary.simpleMessage("non disponibile"),
    "update" : MessageLookupByLibrary.simpleMessage("Aggiornare"),
    "updateUserInfor" : MessageLookupByLibrary.simpleMessage("Aggiorna il profilo"),
    "username" : MessageLookupByLibrary.simpleMessage("Nome utente"),
    "verifySMSCode" : MessageLookupByLibrary.simpleMessage("Verificare"),
    "video" : MessageLookupByLibrary.simpleMessage("video"),
    "vietnamese" : MessageLookupByLibrary.simpleMessage("Vietnam"),
    "weFoundBlogs" : MessageLookupByLibrary.simpleMessage("Abbiamo trovato blog"),
    "weFoundProducts" : m6,
    "webView" : MessageLookupByLibrary.simpleMessage("Visualizzazione Web"),
    "welcome" : MessageLookupByLibrary.simpleMessage("benvenuto"),
    "womanCollections" : MessageLookupByLibrary.simpleMessage("Collezioni donna"),
    "writeComment" : MessageLookupByLibrary.simpleMessage("Scrivi il tuo commento"),
    "writeYourNote" : MessageLookupByLibrary.simpleMessage("Scrivi la tua nota"),
    "yearsAgo" : m7,
    "youHavePoints" : MessageLookupByLibrary.simpleMessage("Hai \$ punti punti"),
    "youMightAlsoLike" : MessageLookupByLibrary.simpleMessage("Potrebbe piacerti anche"),
    "yourBagIsEmpty" : MessageLookupByLibrary.simpleMessage("La tua borsa è vuota"),
    "yourNote" : MessageLookupByLibrary.simpleMessage("La tua nota"),
    "zipCode" : MessageLookupByLibrary.simpleMessage("Cap"),
    "zipCodeIsRequired" : MessageLookupByLibrary.simpleMessage("Il campo del codice postale è obbligatorio")
  };
}
