// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a zh locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'zh';

  static m0(day) => "${day} 天前";

  static m1(hour) => "${hour} 小时前";

  static m2(minute) => "${minute} 分钟前";

  static m3(month) => "${month} 个月前";

  static m4(percent) => "销售额 ${percent} %";

  static m5(second) => "${second} 秒前";

  static m6(length) => "我们找到 ${length} 个产品";

  static m7(year) => "${year} 年前";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "aboutUs" : MessageLookupByLibrary.simpleMessage("关于我们"),
    "addANewPost" : MessageLookupByLibrary.simpleMessage("添加新帖子"),
    "addNewBlog" : MessageLookupByLibrary.simpleMessage("添加新博客"),
    "addToCart" : MessageLookupByLibrary.simpleMessage("添加到购物车"),
    "addToCartSucessfully" : MessageLookupByLibrary.simpleMessage("已添加到您的购物车"),
    "additionalInformation" : MessageLookupByLibrary.simpleMessage("附加信息"),
    "address" : MessageLookupByLibrary.simpleMessage("地址"),
    "agreeWithPrivacy" : MessageLookupByLibrary.simpleMessage("隐私权和条款"),
    "all" : MessageLookupByLibrary.simpleMessage("所有"),
    "apply" : MessageLookupByLibrary.simpleMessage("应用"),
    "arabic" : MessageLookupByLibrary.simpleMessage("阿拉伯"),
    "attributes" : MessageLookupByLibrary.simpleMessage("属性"),
    "availability" : MessageLookupByLibrary.simpleMessage("可用性"),
    "backToShop" : MessageLookupByLibrary.simpleMessage("回到商店"),
    "bagsCollections" : MessageLookupByLibrary.simpleMessage("齿轮收藏"),
    "blog" : MessageLookupByLibrary.simpleMessage("博客"),
    "brazil" : MessageLookupByLibrary.simpleMessage("巴西"),
    "buyNow" : MessageLookupByLibrary.simpleMessage("立即购买"),
    "byCategory" : MessageLookupByLibrary.simpleMessage("按类别"),
    "byPrice" : MessageLookupByLibrary.simpleMessage("按价格"),
    "cancel" : MessageLookupByLibrary.simpleMessage("取消"),
    "cart" : MessageLookupByLibrary.simpleMessage("大车"),
    "categories" : MessageLookupByLibrary.simpleMessage("分类"),
    "category" : MessageLookupByLibrary.simpleMessage("类别"),
    "checkout" : MessageLookupByLibrary.simpleMessage("查看"),
    "chinese" : MessageLookupByLibrary.simpleMessage("中文"),
    "chooseYourPaymentMethod" : MessageLookupByLibrary.simpleMessage("选择你的付款方式"),
    "city" : MessageLookupByLibrary.simpleMessage("市"),
    "cityIsRequired" : MessageLookupByLibrary.simpleMessage("必须填写城市字段"),
    "clear" : MessageLookupByLibrary.simpleMessage("明确"),
    "clearCart" : MessageLookupByLibrary.simpleMessage("清除购物车"),
    "close" : MessageLookupByLibrary.simpleMessage("关"),
    "color" : MessageLookupByLibrary.simpleMessage("颜色"),
    "commentFirst" : MessageLookupByLibrary.simpleMessage("请写您的评论"),
    "commentSuccessfully" : MessageLookupByLibrary.simpleMessage("评论成功，请等待您的评论被批准"),
    "contact" : MessageLookupByLibrary.simpleMessage("联系"),
    "content" : MessageLookupByLibrary.simpleMessage("内容"),
    "continueToPayment" : MessageLookupByLibrary.simpleMessage("继续付款"),
    "continueToReview" : MessageLookupByLibrary.simpleMessage("继续审查"),
    "continueToShipping" : MessageLookupByLibrary.simpleMessage("继续运送"),
    "country" : MessageLookupByLibrary.simpleMessage("国家"),
    "countryIsRequired" : MessageLookupByLibrary.simpleMessage("国家字段为必填项"),
    "couponCode" : MessageLookupByLibrary.simpleMessage("优惠券代码"),
    "couponMsgSuccess" : MessageLookupByLibrary.simpleMessage("恭喜你！优惠券代码已成功应用"),
    "createAnAccount" : MessageLookupByLibrary.simpleMessage("创建一个帐户"),
    "createNewPostSuccessfully" : MessageLookupByLibrary.simpleMessage("您的帖子已成功创建为草稿。请查看您的管理站点。"),
    "createPost" : MessageLookupByLibrary.simpleMessage(""),
    "currencies" : MessageLookupByLibrary.simpleMessage("货币"),
    "currentPassword" : MessageLookupByLibrary.simpleMessage("当前密码"),
    "darkTheme" : MessageLookupByLibrary.simpleMessage("黑暗主题"),
    "date" : MessageLookupByLibrary.simpleMessage("日期"),
    "daysAgo" : m0,
    "description" : MessageLookupByLibrary.simpleMessage("描述"),
    "discount" : MessageLookupByLibrary.simpleMessage("折扣"),
    "displayName" : MessageLookupByLibrary.simpleMessage("显示名称"),
    "done" : MessageLookupByLibrary.simpleMessage("完成"),
    "dontHaveAccount" : MessageLookupByLibrary.simpleMessage("没有帐户？"),
    "dutch" : MessageLookupByLibrary.simpleMessage("荷兰人"),
    "email" : MessageLookupByLibrary.simpleMessage("电子邮件"),
    "emailIsRequired" : MessageLookupByLibrary.simpleMessage("电子邮件字段为必填"),
    "emptyCartSubtitle" : MessageLookupByLibrary.simpleMessage("您似乎还没有在包中添加任何物品。开始购物以填补它。"),
    "emptyComment" : MessageLookupByLibrary.simpleMessage("您的评论不能为空"),
    "emptySearch" : MessageLookupByLibrary.simpleMessage("您尚未搜索商品。现在开始-我们会为您提供帮助。"),
    "emptyWishlistSubtitle" : MessageLookupByLibrary.simpleMessage("轻按产品旁边的任何心脏，即可收藏。我们将在这里为您保存！"),
    "english" : MessageLookupByLibrary.simpleMessage("英语"),
    "enterYourEmail" : MessageLookupByLibrary.simpleMessage("输入你的电子邮箱"),
    "enterYourPassword" : MessageLookupByLibrary.simpleMessage("输入您的密码"),
    "events" : MessageLookupByLibrary.simpleMessage("活动"),
    "featureProducts" : MessageLookupByLibrary.simpleMessage("特色产品"),
    "filter" : MessageLookupByLibrary.simpleMessage("过滤"),
    "firstName" : MessageLookupByLibrary.simpleMessage("名字"),
    "firstNameIsRequired" : MessageLookupByLibrary.simpleMessage("名字字段为必填项"),
    "french" : MessageLookupByLibrary.simpleMessage("法国"),
    "generalSetting" : MessageLookupByLibrary.simpleMessage("通用设置"),
    "german" : MessageLookupByLibrary.simpleMessage("德语"),
    "getNotification" : MessageLookupByLibrary.simpleMessage("得到通知"),
    "goBackToAddress" : MessageLookupByLibrary.simpleMessage("返回地址"),
    "goBackToReview" : MessageLookupByLibrary.simpleMessage("返回查看"),
    "goBackToShipping" : MessageLookupByLibrary.simpleMessage("回到运输"),
    "hebrew" : MessageLookupByLibrary.simpleMessage("希伯来语"),
    "hindi" : MessageLookupByLibrary.simpleMessage("印地语"),
    "home" : MessageLookupByLibrary.simpleMessage("家"),
    "hoursAgo" : m1,
    "hungary" : MessageLookupByLibrary.simpleMessage("匈牙利"),
    "iAgree" : MessageLookupByLibrary.simpleMessage("我同意"),
    "imageFeature" : MessageLookupByLibrary.simpleMessage("图片特征"),
    "inStock" : MessageLookupByLibrary.simpleMessage("有现货"),
    "indonesian" : MessageLookupByLibrary.simpleMessage("印度尼西亚"),
    "invalidSMSCode" : MessageLookupByLibrary.simpleMessage("无效的短信验证码"),
    "italian" : MessageLookupByLibrary.simpleMessage("意大利"),
    "items" : MessageLookupByLibrary.simpleMessage("项目"),
    "itsOrdered" : MessageLookupByLibrary.simpleMessage("订的！"),
    "iwantToCreateAccount" : MessageLookupByLibrary.simpleMessage("我要建立一个帐号"),
    "japanese" : MessageLookupByLibrary.simpleMessage("日本"),
    "korean" : MessageLookupByLibrary.simpleMessage("朝鲜的"),
    "language" : MessageLookupByLibrary.simpleMessage("语言"),
    "languageSuccess" : MessageLookupByLibrary.simpleMessage("语言已成功更新"),
    "lastName" : MessageLookupByLibrary.simpleMessage("姓"),
    "lastNameIsRequired" : MessageLookupByLibrary.simpleMessage("姓氏字段为必填项"),
    "layout" : MessageLookupByLibrary.simpleMessage("版面"),
    "listMessages" : MessageLookupByLibrary.simpleMessage("通知讯息"),
    "loadFail" : MessageLookupByLibrary.simpleMessage("加载失败！"),
    "loading" : MessageLookupByLibrary.simpleMessage("载入中..."),
    "login" : MessageLookupByLibrary.simpleMessage("登录"),
    "loginToComment" : MessageLookupByLibrary.simpleMessage("请登录后发表评论"),
    "loginToYourAccount" : MessageLookupByLibrary.simpleMessage("登录到您的帐户"),
    "logout" : MessageLookupByLibrary.simpleMessage("登出"),
    "manCollections" : MessageLookupByLibrary.simpleMessage("男士系列"),
    "minutesAgo" : m2,
    "momentAgo" : MessageLookupByLibrary.simpleMessage("刚才"),
    "monthsAgo" : m3,
    "myCart" : MessageLookupByLibrary.simpleMessage("我的车"),
    "myPoints" : MessageLookupByLibrary.simpleMessage("我的观点"),
    "myWishList" : MessageLookupByLibrary.simpleMessage("我的收藏"),
    "newPassword" : MessageLookupByLibrary.simpleMessage("新密码"),
    "next" : MessageLookupByLibrary.simpleMessage("下一个"),
    "niceName" : MessageLookupByLibrary.simpleMessage("好名字"),
    "noData" : MessageLookupByLibrary.simpleMessage("没有更多数据"),
    "noFavoritesYet" : MessageLookupByLibrary.simpleMessage("尚无收藏。"),
    "noInternetConnection" : MessageLookupByLibrary.simpleMessage("没有网络连接"),
    "noOrders" : MessageLookupByLibrary.simpleMessage("没有订单"),
    "noPost" : MessageLookupByLibrary.simpleMessage("您尚未创建任何博客！"),
    "noProduct" : MessageLookupByLibrary.simpleMessage("没有产品"),
    "noReviews" : MessageLookupByLibrary.simpleMessage("没有评论"),
    "or" : MessageLookupByLibrary.simpleMessage("要么"),
    "orderDate" : MessageLookupByLibrary.simpleMessage("订购日期"),
    "orderDetail" : MessageLookupByLibrary.simpleMessage("订单详细信息"),
    "orderHistory" : MessageLookupByLibrary.simpleMessage("订单历史"),
    "orderNo" : MessageLookupByLibrary.simpleMessage("订单号。"),
    "orderNotes" : MessageLookupByLibrary.simpleMessage("订购须知"),
    "orderSuccessMsg1" : MessageLookupByLibrary.simpleMessage("您可以使用我们的交货状态功能检查订单状态。您将收到订单确认电子邮件，其中包含您的订单详细信息以及跟踪其进度的链接。"),
    "orderSuccessMsg2" : MessageLookupByLibrary.simpleMessage("您可以使用之前定义的电子邮件和密码登录到您的帐户。在您的帐户上，您可以编辑个人资料数据，检查交易历史记录，编辑对新闻通讯的订阅。"),
    "orderSuccessTitle1" : MessageLookupByLibrary.simpleMessage("您已成功下订单"),
    "orderSuccessTitle2" : MessageLookupByLibrary.simpleMessage("你的帐户"),
    "outOfStock" : MessageLookupByLibrary.simpleMessage("缺货"),
    "pageView" : MessageLookupByLibrary.simpleMessage("页面预览"),
    "password" : MessageLookupByLibrary.simpleMessage("密码"),
    "payment" : MessageLookupByLibrary.simpleMessage("付款"),
    "paymentMethod" : MessageLookupByLibrary.simpleMessage("付款方法"),
    "paymentMethods" : MessageLookupByLibrary.simpleMessage("支付方式"),
    "phoneIsRequired" : MessageLookupByLibrary.simpleMessage("电话号码字段为必填项"),
    "phoneNumber" : MessageLookupByLibrary.simpleMessage("电话号码"),
    "placeMyOrder" : MessageLookupByLibrary.simpleMessage("下订单"),
    "pleaseInput" : MessageLookupByLibrary.simpleMessage("请填写所有字段"),
    "point" : MessageLookupByLibrary.simpleMessage("点"),
    "postManagement" : MessageLookupByLibrary.simpleMessage("邮政管理"),
    "privacyAndTerm" : MessageLookupByLibrary.simpleMessage("隐私权和条款"),
    "privacyPolicy" : MessageLookupByLibrary.simpleMessage("隐私政策"),
    "productAdded" : MessageLookupByLibrary.simpleMessage("该产品已添加"),
    "productRating" : MessageLookupByLibrary.simpleMessage("你的评分"),
    "products" : MessageLookupByLibrary.simpleMessage("制品"),
    "pullToLoadMore" : MessageLookupByLibrary.simpleMessage("拉动加载更多"),
    "rateTheApp" : MessageLookupByLibrary.simpleMessage("评价应用"),
    "ratingFirst" : MessageLookupByLibrary.simpleMessage("请先评分，然后再发送评论"),
    "readReviews" : MessageLookupByLibrary.simpleMessage("评测"),
    "recentSearches" : MessageLookupByLibrary.simpleMessage("历史"),
    "recentView" : MessageLookupByLibrary.simpleMessage("您最近的观点"),
    "recents" : MessageLookupByLibrary.simpleMessage("最近"),
    "refundRequest" : MessageLookupByLibrary.simpleMessage("退款要求"),
    "releaseToLoadMore" : MessageLookupByLibrary.simpleMessage("释放以加载更多"),
    "remove" : MessageLookupByLibrary.simpleMessage("去掉"),
    "reset" : MessageLookupByLibrary.simpleMessage("重启"),
    "review" : MessageLookupByLibrary.simpleMessage("评论"),
    "romanian" : MessageLookupByLibrary.simpleMessage("罗马尼亚"),
    "russian" : MessageLookupByLibrary.simpleMessage("俄语"),
    "sale" : m4,
    "saveAddress" : MessageLookupByLibrary.simpleMessage("保存地址"),
    "saveAddressSuccess" : MessageLookupByLibrary.simpleMessage("您的地址存在于您的本地"),
    "saveToWishList" : MessageLookupByLibrary.simpleMessage("保存到收藏夹"),
    "search" : MessageLookupByLibrary.simpleMessage("搜索"),
    "searchForItems" : MessageLookupByLibrary.simpleMessage("搜索物品"),
    "searchInput" : MessageLookupByLibrary.simpleMessage("请在搜索栏中输入内容"),
    "searchingAddress" : MessageLookupByLibrary.simpleMessage("搜索地址"),
    "secondsAgo" : m5,
    "seeAll" : MessageLookupByLibrary.simpleMessage("查看全部"),
    "selectAddress" : MessageLookupByLibrary.simpleMessage("选择地址"),
    "selectTheColor" : MessageLookupByLibrary.simpleMessage("选择颜色"),
    "selectTheQuantity" : MessageLookupByLibrary.simpleMessage("选择数量"),
    "selectTheSize" : MessageLookupByLibrary.simpleMessage("选择尺寸"),
    "sendSMSCode" : MessageLookupByLibrary.simpleMessage("获取代码"),
    "settings" : MessageLookupByLibrary.simpleMessage("设置"),
    "share" : MessageLookupByLibrary.simpleMessage("分享"),
    "shipping" : MessageLookupByLibrary.simpleMessage("运输"),
    "shippingAddress" : MessageLookupByLibrary.simpleMessage("收件地址"),
    "shippingMethod" : MessageLookupByLibrary.simpleMessage("邮寄方式"),
    "shop" : MessageLookupByLibrary.simpleMessage("店"),
    "showAllMyOrdered" : MessageLookupByLibrary.simpleMessage("显示我所有的订单"),
    "showGallery" : MessageLookupByLibrary.simpleMessage("展览馆"),
    "signIn" : MessageLookupByLibrary.simpleMessage("登入"),
    "signInWithEmail" : MessageLookupByLibrary.simpleMessage("使用电子邮件登录"),
    "signUp" : MessageLookupByLibrary.simpleMessage("注册"),
    "signup" : MessageLookupByLibrary.simpleMessage("注册"),
    "size" : MessageLookupByLibrary.simpleMessage("尺寸"),
    "spanish" : MessageLookupByLibrary.simpleMessage("西班牙语"),
    "startExploring" : MessageLookupByLibrary.simpleMessage("开始探索"),
    "startShopping" : MessageLookupByLibrary.simpleMessage("开始购物"),
    "stateIsRequired" : MessageLookupByLibrary.simpleMessage("状态字段为必填项"),
    "stateProvince" : MessageLookupByLibrary.simpleMessage("州/省"),
    "status" : MessageLookupByLibrary.simpleMessage("状态"),
    "streetIsRequired" : MessageLookupByLibrary.simpleMessage("街道名称字段为必填项"),
    "streetName" : MessageLookupByLibrary.simpleMessage("街道名称"),
    "submit" : MessageLookupByLibrary.simpleMessage("提交"),
    "subtotal" : MessageLookupByLibrary.simpleMessage("小计"),
    "tags" : MessageLookupByLibrary.simpleMessage("标签"),
    "thailand" : MessageLookupByLibrary.simpleMessage("泰国"),
    "title" : MessageLookupByLibrary.simpleMessage("标题"),
    "total" : MessageLookupByLibrary.simpleMessage("总"),
    "totalTax" : MessageLookupByLibrary.simpleMessage("总税"),
    "trackingNumberIs" : MessageLookupByLibrary.simpleMessage("追踪号码为"),
    "trackingPage" : MessageLookupByLibrary.simpleMessage("追踪页面"),
    "turkish" : MessageLookupByLibrary.simpleMessage("土耳其"),
    "unavailable" : MessageLookupByLibrary.simpleMessage("不可用"),
    "update" : MessageLookupByLibrary.simpleMessage("更新"),
    "updateUserInfor" : MessageLookupByLibrary.simpleMessage("更新配置文件"),
    "username" : MessageLookupByLibrary.simpleMessage("用户名"),
    "verifySMSCode" : MessageLookupByLibrary.simpleMessage("校验"),
    "video" : MessageLookupByLibrary.simpleMessage("视频"),
    "vietnamese" : MessageLookupByLibrary.simpleMessage("越南"),
    "weFoundBlogs" : MessageLookupByLibrary.simpleMessage("我们找到了博客"),
    "weFoundProducts" : m6,
    "webView" : MessageLookupByLibrary.simpleMessage("网页浏览"),
    "welcome" : MessageLookupByLibrary.simpleMessage("欢迎"),
    "womanCollections" : MessageLookupByLibrary.simpleMessage("女人系列"),
    "writeComment" : MessageLookupByLibrary.simpleMessage("写你的评论"),
    "writeYourNote" : MessageLookupByLibrary.simpleMessage("写你的笔记"),
    "yearsAgo" : m7,
    "youHavePoints" : MessageLookupByLibrary.simpleMessage("您有\$ point积分"),
    "youMightAlsoLike" : MessageLookupByLibrary.simpleMessage("您可能还喜欢"),
    "yourBagIsEmpty" : MessageLookupByLibrary.simpleMessage("你的包是空的"),
    "yourNote" : MessageLookupByLibrary.simpleMessage("你的笔记"),
    "zipCode" : MessageLookupByLibrary.simpleMessage("邮政编码"),
    "zipCodeIsRequired" : MessageLookupByLibrary.simpleMessage("邮递区号为必填项")
  };
}
